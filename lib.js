
var jira;
var csvtojson = require('csvtojson');
var fs = require('fs');

module.exports =
{
setJiraInstance: function(obj){
	jira = obj;
},

 async convertCSVtoJSON(filepath)
 {
    var wsa11yIssues = [];

    return new Promise(function(resolve, reject){
     csvtojson()
      .fromFile(filepath)
         .on('json',(jsonObj)=>{
                wsa11yIssues.push(jsonObj);
      })
      .on('done',(error)=>{
               if(error){
                console.log('Error while converting csv data to json format, error : ' + error);
                reject(error);
              } else {
                resolve(wsa11yIssues);
              };
      });
    });
 },

async processDirectory(config)
{
	var filesmap = [];
	return new Promise(function(resolve, reject){
		  fs.readdir(config.directoryPath, function(err, files){
		    files.forEach(function(file){
		       if (file.endsWith('.csv') || file.endsWith('.json')){
		       	 filesmap.push(file);
		       }
		    });
		    resolve(filesmap);
  		  });
	});  
},

// Get screens

async getScreens()
{
  return await jira.doRequest(jira.makeRequestHeader(jira.makeUri({
      pathname: '/screens',
    })));
},

async getTabsForScreen(projKey, screenId)
{
	//var prj = {"projectKey": projKey}; // commenting this as this filtering doesn't work for some jira boards
	var path = '/screens/'+ screenId +'/tabs';
	return await jira.doRequest(jira.makeRequestHeader(jira.makeUri({
      pathname: path,
    }) 
    /*,
    { 
 		method: 'GET',
 		body: prj,
    } */
    ));
},

// Add field to the respective tab of the screen
async addFieldToTab(screenId, tabId, field)
{
	var wsField = {"fieldId": field.id};
	var path = '/screens/' + screenId + '/tabs/' + tabId + '/fields';
   
   var fieldslist = await jira.doRequest(jira.makeRequestHeader(jira.makeUri({
      pathname: path,
  	})));

    var fieldExists = false;
    fieldslist.forEach(function(sfield){
    	if (sfield.id == field.id){
    		fieldExists = true;
    	}
    });

    if (!fieldExists){
		return await jira.doRequest(jira.makeRequestHeader(jira.makeUri({
	      pathname: path}) , {
		  method:'POST',
		  body: wsField,
		   },
	    ));
	}
},

// Associate screens for choosen custom field
async getProjectScreens(projKey, callback){
  var screenCollection = [];

  await this.getScreens()
  .then(function(projScreens){
		projScreens.values.forEach(function(screen){
       		//if (screen.name.startsWith(projKey+":")){ // commenting this as this projkey filtering doesn't work for some jira boards
       			 //get screen tabs
       			 screenCollection.push(screen);
       		//};
  		});
	})
	.catch(function(err){
		console.log("error while fetching screens list : " + err);
		process.exit();
	});
	callback(screenCollection);
},

// get tabs for each screen
async getTabs(lastScreen, projKey, screen, callback){
 var tabsCollection = [];
 //await this.getTabsForScreen(projKey,screen.id) // commenting this as this projkey filtering doesn't work for some jira boards
 await this.getTabsForScreen(projKey, screen.id) 
	.then(function(tabs){
		tabsCollection.push(tabs);
	})
	.catch(function(err){
		//console.log("error while fetching tabs, error : " + err);
		//process.exit();
   });
  callback(lastScreen, screen, tabsCollection);
},

//add field to the screen tab
async addFieldToScreenTab(lastScreen, lastTab, screen, tabId, field, callback){
   await this.addFieldToTab(screen.id,tabId,field)
   .then(function(addedField){
   		//console.log("Successfully configured the custom field for project screen '"+ screen.name +"'");
   		if (lastScreen && lastTab){
   			//callback();
   			setTimeout(function(){ callback(); }, 1000);
   		};
   })
   .catch(function(err){
   	 console.log("error while adding field : " + err);
   	 process.exit();
   });
},

// Validate JIRA connection
async validateCredentials()
{
	var projs = [];
	await this.getProjects()
	.then(function(projects){
		projects.forEach(function(project){
       		projs.push(project);
  		});
	})
	.catch(function(err){
		 throw err;
	});
},

// Get projects
async getProjects()
{
  return await jira.doRequest(jira.makeRequestHeader(jira.makeUri({
      pathname: '/project',
    })));
},

async getProjectsList(callback){
	var projs = [];
	await this.getProjects()
	.then(function(projects){
		projects.forEach(function(project){
       		projs.push(project);
  		});
	})
	.catch(function(err){
		console.log("error while fetching projects list : " + err);
		process.exit();
	});
	 
	callback(projs);
},

// Create a custom feld
async createCustomField(cfield, callback){

 	var returnField; 
	var cfieldFound = false;

	 await jira.listFields()
	.then(function(customfields){

	   customfields.forEach(function(customField){
	     if (customField.name.trim() == cfield.name){
	     	 //console.log('field found');
	         //return customField; //need to check why this code is not working

	         returnField = customField;
	         cfieldFound = true; 
	     }
	   })
	});
	   
	if (!cfieldFound){
		   // no custom field found with this name
		   //console.log("Creating custom field..." + "\r");

		    await jira.createCustomField(cfield)
			.then(function(field){
				    //console.log("Created custom field successfully with name : " + field.name +"\r");
				    returnField = field;
			})
			.catch(function(err){
				    console.log("error in creating custom field '"+ cfield.name + "': " + err);
				    process.exit();
		    });
	  }
	  callback(returnField); 
},

// Check for existence of the custom field
async doesCustomFieldExists(customField){
	jira.listFields()
	.then(function(cfields){
	   cfields.forEach(function(cfield){
	     if (cfield.name.trim() == customField.name){
	         return cfield;
	     }
	   })
	   return null;
    })
	.catch(function(err){
	  console.log("error in checking for the existence of a custom field '" + customField.name + "': " + err);
	  process.exit();
	}); 
}

}